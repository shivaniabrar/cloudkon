package main

import (
	"encoding/json"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	//"github.com/aws/aws-sdk-go/service/sqs/sqsiface"

)

type JobMessage struct {
   Id   string
   Body string
}

func main() {
    svc := sqs.New(session.New())
    params := &sqs.CreateQueueInput{
        QueueName: aws.String("Jobs"),
    }
    svc.CreateQueue(params)
    
    params1 := &sqs.GetQueueUrlInput{
	QueueName:              aws.String("Jobs"),
    }
    resp, err := svc.GetQueueUrl(params1)
    
    mymessage := JobMessage {Id : "1", Body: "100"}
    msg,err := json.Marshal(mymessage);
    params2 := &sqs.SendMessageInput{
	MessageBody:  aws.String(string(msg)),
	QueueUrl:     aws.String(*resp.QueueUrl), // Required
	DelaySeconds: aws.Int64(1),
    }
    _, err = svc.SendMessage(params2)
    
    respm, err := svc.ReceiveMessage(&sqs.ReceiveMessageInput{
			QueueUrl:          aws.String(*resp.QueueUrl),
			VisibilityTimeout: aws.Int64(0),
			WaitTimeSeconds:   aws.Int64(20),
		})
        
    for _, msg := range respm.Messages {
			result := &JobMessage {}
			if err := json.Unmarshal([]byte(aws.StringValue(msg.Body)), result); err != nil {
				fmt.Println("Failed to unmarshal message", err)
				continue
			}
            fmt.Print(result.Id)
            svc.DeleteMessage(&sqs.DeleteMessageInput{
				QueueUrl:      aws.String(*resp.QueueUrl)  ,
				ReceiptHandle: msg.ReceiptHandle,
			})
			return
		}
    
 
    if err != nil {
	// Print the error, cast err to awserr.Error to get the Code and
	// Message from an error.
	fmt.Println(err.Error())
	return
    }
}