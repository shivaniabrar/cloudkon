package executors

import (
    "sync"
    "strings"
    "os/exec"
    "errors"
    "fmt"
) 

type ExecutorPool struct {
    executors []*Executor
    wg sync.WaitGroup
}

type Result struct{
    Id int
    Cmdline string
    Status string
}

func (result Result) Result() string {
    return result.Status
}

type Task interface {
    Cmd() string 
    GetId() int
}

type Executor struct {
    id int
    resultCh chan interface{}
    queue chan interface{}
}

func NewExecutorPool(size int, resultCh chan interface{}, queue chan interface{}) *ExecutorPool {
    pool := &ExecutorPool {
        executors: make([]*Executor, size),
    }
    
    for i := 0; i < len(pool.executors); i++ {
		pool.wg.Add(1)
		pool.executors[i] = NewExecutor(i, resultCh, queue)

		go func(executor *Executor) {
			executor.run()
			pool.wg.Done()
		}(pool.executors[i])
	}

	return pool
}

func (e *ExecutorPool) WaitForExecutorsDone() {
	e.wg.Wait()
}


func NewExecutor(id int, resultCh chan interface{}, queue chan interface{}) *Executor {
    return &Executor{id: id, resultCh: resultCh, queue: queue}
}

func (e *Executor) run() {
	status := ""
    for {
	        job, end := <- e.queue
            if !end {
                fmt.Println("Done")
            }
			output, err := e.processJob(job.(Task).Cmd())
			
            if err != nil {
                status = "Failed"
			} else {
                status = "Success"
			}
            
            result := Result{Id: job.(Task).GetId(), Cmdline: job.(Task).Cmd(), Status : status + " " + output }
            e.resultCh <- result
		}
}

func (e *Executor) processJob(job string) (output string, err error) {
    var out []byte
    output = ""
    if job == "" {
        return output, errors.New("No Job")
    }
    
    cmd := strings.Fields(job)
    cmdName := cmd[0]
    cmdArgs := cmd[1:]
    
    if out, err = exec.Command(cmdName, cmdArgs...).Output(); err != nil {
		fmt.Println(err)
        return output, err
	}
    output = string(out)
    return
}