package workers

import (
      "executors" 
      "github.com/aws/aws-sdk-go/aws"
	  "github.com/aws/aws-sdk-go/aws/session"
	  "github.com/aws/aws-sdk-go/service/sqs"
      "log"
      "encoding/json"
      "fmt"
      "strconv"
)

type Worker struct {
    queueName string
    Task chan string
    Result chan string
    queueSize int
    nthreads int
}

type Task struct {
    Id int
    CmdLine string
    Status string
}

func (task Task) Cmd() string {
    return task.CmdLine
}

func (task Task) GetId() int {
    return task.Id
}

func (task Task) Result(result string) int {
    task.Status = result
    return 0
}

func NewWorker(queueName string, nthreads int, queueSize int) (* Worker) {
    worker := &Worker{ queueName : queueName, 
                       queueSize : queueSize,
                       nthreads  : nthreads,
                       Task : make(chan string, queueSize),
                       Result : make(chan string, queueSize),
                     }    
    return worker
}

func (worker *Worker) Run() {
    if worker.queueName == "LOCAL" {
        go worker.RunLocalWorker()
    } else {
        go worker.RunRemoteWorker()  
    }
}

func (worker *Worker) RunLocalWorker() {
    resultCh := make(chan interface{}, 10)
    queue := make(chan interface{}, 10)
    task_executors := executors.NewExecutorPool(worker.nthreads, resultCh, queue)   
    cnt := 0
    go func () {
        for tasks := range worker.Task {
            if tasks == "" {
                continue
            }
            cnt += 1
            queue <- Task{Id: cnt, CmdLine: tasks}
        }
    }()
    go func (resultCh chan interface{}) {
        for jobs := range resultCh {
            if jobs == nil {
                continue
            }
            task := jobs.(executors.Result)
            task_id  := strconv.Itoa(task.Id)
            status := "task_id(" + task_id + "):" + task.Status
            worker.Result <- status
        }
    }(resultCh)
    task_executors.WaitForExecutorsDone()
}

func (worker *Worker) RunRemoteWorker() {
    dict := make(map[int]bool)
    done := make(chan bool)
    resultQueueName := "Results"
    svc := sqs.New(session.New())
    
    err := SetupSQSQueue(svc, worker.queueName)
    Print_And_Exit_If_Error(err)
   
    jobsQueueURL, err := GetSQSQueueUrl(svc, worker.queueName)
    Print_And_Exit_If_Error(err)
    
    err = SetupSQSQueue(svc, resultQueueName)
    Print_And_Exit_If_Error(err)
    
    resultQueueURL, err := GetSQSQueueUrl(svc, resultQueueName)
    Print_And_Exit_If_Error(err)
    
    go func () {
        cnt := 1
        for tasks := range worker.Task {
             msg, err := SQSMessage(jobsQueueURL, &Task{CmdLine: tasks, Id: cnt})
              Print_And_Exit_If_Error(err)
              _, err = svc.SendMessage(msg)
               Print_And_Exit_If_Error(err)
               fmt.Println("Sending Msg:", tasks, cnt)
               dict[cnt] = true
               cnt += 1
        }
        done <- true
    }()
    <- done
    
    for 0 < 1 { 
           msgId, status := SQSResultMessage(svc, resultQueueURL)
           _, isKeyPresent := dict[msgId]
           if !isKeyPresent {
               continue
           }
           delete(dict,msgId)
           task_id := strconv.Itoa(msgId)
           status = "task_id(" + task_id + "):" + status          
           worker.Result <- status
    }
    
    return
}

func SQSResult(svc *sqs.SQS, queueName string) (result *Task) {
    resp, err := svc.ReceiveMessage(&sqs.ReceiveMessageInput{
			QueueUrl:          aws.String(queueName),
			VisibilityTimeout: aws.Int64(0),
			WaitTimeSeconds:   aws.Int64(0),
		})
   Print_And_Exit_If_Error(err)
   
   for _, msg := range resp.Messages {
			result = &Task{}
			if err := json.Unmarshal([]byte(aws.StringValue(msg.Body)), result); err != nil {
				break
			}
         
            svc.DeleteMessage(&sqs.DeleteMessageInput{
				QueueUrl:      aws.String(queueName)  ,
				ReceiptHandle: msg.ReceiptHandle,
			})
    }
    return
}

func SQSResultMessage(svc *sqs.SQS, queueName string) (msgId int, status string){
    resp, err := svc.ReceiveMessage(&sqs.ReceiveMessageInput{
			QueueUrl:          aws.String(queueName),
			VisibilityTimeout: aws.Int64(0),
			WaitTimeSeconds:   aws.Int64(0),
		})
   Print_And_Exit_If_Error(err)
   
   for _, msg := range resp.Messages {
			result := &Task{}
			if err := json.Unmarshal([]byte(aws.StringValue(msg.Body)), result); err != nil {
				break
			}
            msgId = result.Id
            status = result.Status
         
            svc.DeleteMessage(&sqs.DeleteMessageInput{
				QueueUrl:      aws.String(queueName)  ,
				ReceiptHandle: msg.ReceiptHandle,
			})
    }
    return
}

func SQSMessage (queueName string, task *Task) (*sqs.SendMessageInput, error){
    msg,err := json.Marshal(*task);
    if err != nil {
        return &sqs.SendMessageInput{}, err
    }
    
    return &sqs.SendMessageInput{
	MessageBody:  aws.String(string(msg)),
	QueueUrl:     aws.String(queueName),
	DelaySeconds: aws.Int64(0),
    }, err
}

func SQSMessageResult (queueName string, task *executors.Result) (*sqs.SendMessageInput, error){
    msg,err := json.Marshal(*task);
    if err != nil {
        return &sqs.SendMessageInput{}, err
    }
    
    return &sqs.SendMessageInput{
	MessageBody:  aws.String(string(msg)),
	QueueUrl:     aws.String(queueName),
	DelaySeconds: aws.Int64(0),
    }, err
}

func Print_And_Exit_If_Error (err error) {
     if err != nil {
        log.Fatal(err)
    }
}

func SetupSQSQueue (svc *sqs.SQS,queueName string) (error){
    _, err := svc.CreateQueue(&sqs.CreateQueueInput{
        QueueName: aws.String(queueName),
    })    
    return err
}

func GetSQSQueueUrl (svc *sqs.SQS, queueName string) (string, error){
    resp, err := svc.GetQueueUrl(&sqs.GetQueueUrlInput{
	    QueueName: aws.String(queueName),
    })
    
    if err != nil {
        return "", err
    }
    return *resp.QueueUrl, err
}

