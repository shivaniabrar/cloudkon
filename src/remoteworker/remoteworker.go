package main

import (
    "runtime"
    "workers"
    "flag"
    "strconv"
    "executors"
    "github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
    "github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
    "github.com/aws/aws-sdk-go/service/dynamodb"
)

type Record struct {
    TASKID string
}

func main() {
    
     queueName, nthreads := get_user_parameters()
     resultCh := make(chan interface{}, 10)
     queue := make(chan interface{}, 10)
     task_executors := executors.NewExecutorPool(nthreads, resultCh, queue)   
     resultQueueName := "Results"
     
     svc := sqs.New(session.New())
     dynamosvc := dynamodb.New(session.New())
     err := workers.SetupSQSQueue(svc, queueName)
     
     jobsQueueURL, err := workers.GetSQSQueueUrl(svc, queueName)
     workers.Print_And_Exit_If_Error(err)
     
     err = workers.SetupSQSQueue(svc, resultQueueName)
     workers.Print_And_Exit_If_Error(err)
     
    resultQueueURL, err := workers.GetSQSQueueUrl(svc, resultQueueName)
    workers.Print_And_Exit_If_Error(err)
     
    go func () {
        for 1 < 2 {
            //Get and delete the message from Queue
            task := workers.SQSResult(svc, jobsQueueURL)
            if task == nil {
                continue
            }
            
            record := Record {
                TASKID: strconv.Itoa(task.Id),
            }

            av, err := dynamodbattribute.MarshalMap(record)  
            workers.Print_And_Exit_If_Error(err)          
            key, err := dynamosvc.GetItem(&dynamodb.GetItemInput {
                TableName: aws.String("test"),
                ConsistentRead: aws.Bool(true),
                Key: av,
            }) 
            workers.Print_And_Exit_If_Error(err)
            task_id := ""
            dynamodbattribute.Unmarshal(key.Item["TASKID"], &task_id)
            if task_id == strconv.Itoa(task.Id) {
                continue
            }
            
            dynamosvc.PutItem(&dynamodb.PutItemInput {
                TableName: aws.String("test"),
                Item: av,
            }) 
           
            queue <- task
        }
    }()
    
     go func (resultCh chan interface{}) {
        for jobs := range resultCh {
            if jobs == nil {
                continue
            }
            job := jobs.(executors.Result)
            msg, err := workers.SQSMessageResult(resultQueueURL, &job)
            workers.Print_And_Exit_If_Error(err)
            _, err = svc.SendMessage(msg)
            workers.Print_And_Exit_If_Error(err)
        }
    }(resultCh)
    
    task_executors.WaitForExecutorsDone()
}

func
get_user_parameters() (string, int) {
    queueName := flag.String("s", "LOCAL", "Queue Name")
    nthreads := flag.Int("t", runtime.NumCPU(), "Number of threads")
    flag.Parse()
	return *queueName, *nthreads
}
