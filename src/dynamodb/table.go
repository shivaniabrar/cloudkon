package main

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"os"
    "flag"
    
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func main() {
    tableName, operation := get_user_parameters()
	svc := dynamodb.New(session.New())
    
    switch (operation){
    case "create":
        createTable(svc, tableName)
    case "delete":
        deleteTable(svc, tableName)
    default: 
        fmt.Println("Please enter right operation")
        os.Exit(1)
    }
}

func
createTable(svc *dynamodb.DynamoDB, tableName string) {
    if _, err := svc.CreateTable(&dynamodb.CreateTableInput{
		TableName: aws.String(tableName),
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("TASKID"),
				AttributeType: aws.String(dynamodb.ScalarAttributeTypeS),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("TASKID"),
				KeyType:       aws.String("HASH"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(1),
			WriteCapacityUnits: aws.Int64(1),
		},
	}); err != nil {
		fmt.Println("failed to create Amazon DynamoDB table,", err)
		os.Exit(1)
	}

	fmt.Println("succesffully created", tableName)
}

func
deleteTable(svc *dynamodb.DynamoDB, tableName string) {
    if _, err := svc.DeleteTable(&dynamodb.DeleteTableInput{
	    TableName: aws.String(tableName),
    }); err != nil {
        fmt.Println("Failed to delete Amazon DynamoDB table", err)
		os.Exit(1)
    }
}

//User parameters from command line arguments
func
get_user_parameters() (string, string) {
	tableName := flag.String("t", "test", "DynamoDB Table Name")
    operation := flag.String("o", "create", "Create or Delete table")
    flag.Parse()
	return *tableName, *operation
}
