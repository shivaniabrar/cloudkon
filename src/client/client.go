package main

import (
    "fmt"
    "runtime"
    "os"
    "bufio"
    "workers"
    "log"
    "flag"
    "time"
)

func main() {
    var results []string
    result := ""
    workloadFileName, queueName, nthreads := get_user_parameters()
    numofTask := 0
    queueSize := 10
   
    file, err := os.Open(workloadFileName)
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()
    
    worker := workers.NewWorker(queueName, nthreads, queueSize)
    worker.Run()
    fmt.Println("Number of executors", nthreads) 
    scanner := bufio.NewScanner(file)
    start := time.Now()
    for scanner.Scan() {
        worker.Task <- scanner.Text()
        numofTask += 1
    }
    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
    close(worker.Task)
    
    for ntasks:= 0; ntasks < numofTask; ntasks +=1 {
           result = <- worker.Result
           results = append(results, result)
    }
    duration := time.Since(start)
    fmt.Println(results)
    fmt.Println("Duration in seconds:", duration.Seconds())
}


func
get_user_parameters() (string, string, int) {
	workloadFileName := flag.String("w", "workload", "Workload filename")
    queueName := flag.String("s", "LOCAL", "Queue Name")
    nthreads := flag.Int("t", runtime.NumCPU(), "Number of threads")
    flag.Parse()
	return *workloadFileName, *queueName, *nthreads
}
